package com.epam.EpamLocations;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EpamLocationsApplication {

	public static void main(String[] args) {
		SpringApplication.run(EpamLocationsApplication.class, args);
	}

}
